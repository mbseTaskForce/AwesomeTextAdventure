package de.tu_bs.isf.mbse.textadventure.player.commanddsl.validation

import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import com.google.common.collect.Multimaps
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.CombineCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.CommandDslPackage
import java.util.List
import org.eclipse.xtext.validation.Check

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class CommandDslValidator extends AbstractCommandDslValidator {
	@Check
	def checkCombineCommandItems(CombineCommand command) {
		val indexToItem = command.items.indexToElementMap
		val itemToIndexes = HashMultimap.create
		Multimaps.invertFrom(indexToItem, itemToIndexes)
		
		itemToIndexes.asMap.values.forEach[indexes |
			if (indexes.size > 1) {
				indexes.forEach[i | 
					warning("Can't combine an item with itself", 
						CommandDslPackage.Literals.COMBINE_COMMAND__ITEMS, i)
				]
			}
		]
	}
	
	def private <T> Multimap<Integer, T> getIndexToElementMap(List<T> list) {
		val indexToElement = HashMultimap.create
		for (i : 0..<list.size) {
			indexToElement.put(i, list.get(i))
		}
		indexToElement
	}
}
