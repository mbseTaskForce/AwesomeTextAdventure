package de.tu_bs.isf.mbse.textadventure.geodiagram.providers;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.CreateEditPoliciesOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.editpolicy.IEditPolicyProvider;

import de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.parts.DoorEditPart;
import de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.policies.DoorItemSemanticEditPolicy;
import de.tu_bs.isf.mbse.textadventure.geodiagram.policies.DoorItemSemanticEditPolicyCustom;

public class AdventureEditPolicyProvider extends AbstractProvider implements IEditPolicyProvider{

	@Override
	public boolean provides(IOperation operation) {
		boolean result = false;
		if (operation instanceof CreateEditPoliciesOperation) {
			EditPart editPart = ((CreateEditPoliciesOperation)operation).getEditPart();
			result = editPart instanceof DoorEditPart;
		}
		return result;
	}

	@Override
	public void createEditPolicies(EditPart editPart) {
		if (editPart instanceof DoorEditPart) {
			editPart.installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
					new DoorItemSemanticEditPolicyCustom());
        }
	}

}
