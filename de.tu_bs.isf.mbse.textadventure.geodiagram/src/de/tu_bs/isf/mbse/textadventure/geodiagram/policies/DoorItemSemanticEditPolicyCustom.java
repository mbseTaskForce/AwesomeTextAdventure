package de.tu_bs.isf.mbse.textadventure.geodiagram.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

import de.tu_bs.isf.mbse.textadventure.adventure.Door;
import de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.commands.DoorConnectionCreateCommand;
import de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.commands.DoorConnectionReorientCommand;
import de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.policies.DoorItemSemanticEditPolicy;


public class DoorItemSemanticEditPolicyCustom
		extends
		DoorItemSemanticEditPolicy {


	public DoorItemSemanticEditPolicyCustom() {
		super();
	}

	protected Command getStartCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.providers.AdventureElementTypes.DoorConnection_4001 == req
				.getElementType()) {
			if(((Door)req.getSource()).getConnection() == null){
				return getGEFWrapper(new DoorConnectionCreateCommand(
						req, req.getSource(), req.getTarget()));
			}
		}
		return null;
	}


	protected Command getCompleteCreateRelationshipCommand(
			CreateRelationshipRequest req) {
		if (de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.providers.AdventureElementTypes.DoorConnection_4001 == req
				.getElementType()) {
			if(((Door)req.getTarget()).getConnection() == null){
				return getGEFWrapper(new DoorConnectionCreateCommand(
						req, req.getSource(), req.getTarget()));
			}
		}
		return null;
	}


	protected Command getReorientRelationshipCommand(
			ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case de.tu_bs.isf.mbse.textadventure.adventure.geodiagram.edit.parts.DoorConnectionEditPart.VISUAL_ID:
			return getGEFWrapper(new DoorConnectionReorientCommand(
					req));
		}
		return super.getReorientRelationshipCommand(req);
	}

}
