package de.tu_bs.isf.mbse.textadventure.geodiagram.figures;

import java.io.InputStream;

import org.eclipse.draw2d.ImageFigure;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class StopFigure extends ImageFigure {
	  public StopFigure()  {
		     
		   InputStream in = StopFigure.class.getResourceAsStream("stop.png");
		   Image image = new Image(Display.getDefault(), in);
		   this.setImage(image);
		  
		  } 
		}
