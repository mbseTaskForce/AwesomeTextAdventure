package de.tu_bs.isf.mbse.textadventure.transformation.epsilon;

import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.IEolExecutableModule;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;

/**
 * Provides methods to create EMF models from ECore models
 * @author Gil Engel
 *
 */
public abstract class EpsilonStandalone {
	protected IEolExecutableModule module;

	/**
	 * Use this method to create an EmfModel of an existing ecore model which already exist in RAM
	 * @param Instance of the Adventure
	 * @return
	 * @throws EolModelLoadingException
	 */
	protected EmfModel createEmfModel(EObject adventure) throws EolModelLoadingException{
		

		EmfModel emfModel = new EmfModel();
		emfModel.setModelImpl(adventure.eResource());
		//emfModel.setResource(resource);
		emfModel.setName("Adventure");
		emfModel.setReadOnLoad(true);
		emfModel.setStoredOnDisposal(false);

		return emfModel;

	}
	
	/**
	 * Use this method to create an EmfModel of an existing ecore model which already exist in RAM
	 * @param Instance of the Adventure
	 * @return
	 * @throws EolModelLoadingException
	 */
	protected EmfModel createEmfModel(Resource resource) throws EolModelLoadingException{
		EmfModel emfModel = new EmfModel();
		emfModel.setResource(resource);
		emfModel.setName("Adventure");
		emfModel.setReadOnLoad(true);
		emfModel.setStoredOnDisposal(false);

		return emfModel;

	}	
	/**
	 * Use this method to create an EmfModel of an existing ecore model from file
	 * @param Name of the loaded model. E.g. If you want to access the model via Adventure set name to "Adventure"
	 * @param File uri of the existing model
	 * @param String of all corresponding metamodels. Seperate them with ','
	 * @param readOnLoad use the same configuration as with runtime configuration
	 * @param storeOnDisposal use the same configuration as with runtime configuration
	 * @return the generated model, use this for transformations
	 * @throws EolModelLoadingException
	 * @throws URISyntaxException
	 */
	protected EmfModel createEmfModelByURI(String name, URI model, 
			String metamodel, boolean readOnLoad, boolean storeOnDisposal) 
					throws EolModelLoadingException, URISyntaxException {
		EmfModel emfModel = new EmfModel();
		
		
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, name);
		properties.put(EmfModel.PROPERTY_METAMODEL_URI, metamodel);
		properties.put(EmfModel.PROPERTY_MODEL_URI, model.toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, readOnLoad + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				storeOnDisposal + "");
		emfModel.load(properties, null);
		return emfModel;
	}	
}


