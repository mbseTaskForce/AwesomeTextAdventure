package de.tu_bs.isf.mbse.textadventure.transformation.epsilon;

import java.net.URI;

import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.etl.EtlModule;

import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;

/**
 * Class to perform the ETL transformation from Adventure to Savegame via code
 * @author Gil Engel
 *
 */
public class ETLStandalone extends EpsilonStandalone {
	
	/**
	 * Creates an instance and parses the etl file instantly. Shows problems while parsing in console
	 * @param The ETL file to use
	 * @throws Exception
	 */
	public ETLStandalone(URI etlFile) throws Exception{
		module = new EtlModule();
		module.parse(etlFile);

	    if (module.getParseProblems().size() > 0) {
	        System.err.println("Parse errors occured...");
	        for (ParseProblem problem : module.getParseProblems()) {
	          System.err.println(problem.toString());
	        }
	        return;
	      }
	}
	
	/**
	 * Transform the model to a savegame and saves this model as a file
	 * @param Instance of the Adventure model
	 * @param URI that indicates the file to save
	 * @throws Exception
	 */
	public void transformToSavegame(Adventure adventure, URI savegameFileURI) throws Exception{
		URI metamodel = URI.create("http://tu-bs.de/isf/mbse/textadventure/adventure"); 
		URI metamodel2 = URI.create("http://tu-bs.de/isf/mbse/textadventure/savegame");
		String metamodels = metamodel.toString()+", "+metamodel2.toString();
		
		EmfModel emfModel = createEmfModel(adventure); 
		emfModel.addMetamodelUri(metamodel.toString());
		EmfModel savegame = createEmfModelByURI("Savegame", savegameFileURI, metamodels, false, true);
		
		module.getContext().getModelRepository().addModel(emfModel);
		module.getContext().getModelRepository().addModel(savegame);
		module.execute();
		
		savegame.store(savegameFileURI.toString());	
	}
}
