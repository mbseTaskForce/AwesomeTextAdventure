package de.tu_bs.isf.mbse.textadventure.transformation.commands;

import java.net.URI;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;
import de.tu_bs.isf.mbse.textadventure.transformation.epsilon.ETLStandalone;

public class SavegameTransformationHandler extends SelectionHelperHandler implements IHandler{

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelection selection = HandlerUtil.getCurrentSelectionChecked(event);
		StructuredSelection strucSel = (StructuredSelection)selection;
		
		
		Adventure adventure = getAdventureModelFromSelection(strucSel);		
		if (adventure == null) {
			MessageDialog.openError(PlatformUI.getWorkbench().getDisplay().getActiveShell(), "No adventure found", "Please select an adventure when running this action");
			return null;
		}
		
		XMIResource resource = (XMIResource)adventure.eResource();

		java.net.URI curi = getURIForFileToSave(resource, "savegame");		

		try {
			ETLStandalone etl = new ETLStandalone(URI.create("platform:/plugin/de.tu_bs.isf.mbse.textadventure.transformation/transformations/savegameTransformation.etl"));
			etl.transformToSavegame(adventure, curi);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isHandled() {
		return true;
	}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {
		// TODO Auto-generated method stub
	}

}
