package de.tu_bs.isf.mbse.textadventure.transformation.commands;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.impl.ViewImpl;
import org.eclipse.jface.viewers.StructuredSelection;

import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;

/**
 * Provides methods to get the adventure file from many different possible selections and to create valid URIs in order to save transformed models
 * @author Gil Engel
 *
 */
public abstract class SelectionHelperHandler {
	protected boolean loadFromFile;
	
	/**
	 * Able to detect the adventure from different selections
	 * @param selection
	 * @return
	 */
	public Adventure getAdventureModelFromSelection(StructuredSelection selection){
		loadFromFile = false;
		
		Adventure adventure = null;
		Resource resource = null;
		Object object = selection.getFirstElement();

		if(object instanceof IFile){
			IFile file = (IFile)object;
			
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toString(), false);

		    // Obtain a new resource set
		    ResourceSet resSet = new ResourceSetImpl();

		    // Get the resource
		    resource = (XMIResource)resSet.getResource(fileURI, true);
		    
		    Object obj = resource.getContents().get(0);
		    if(obj instanceof Diagram){
				Diagram diagram = (Diagram)obj;
				adventure = (Adventure)diagram.getElement();
				resource = (XMIResource)adventure.eResource();
				
				loadFromFile = true;
		    }else{
		    	adventure = (Adventure)resource.getContents().get(0);
		    }
		}
		if(object instanceof XMIResourceImpl){
			resource = (XMIResource)object;
			adventure = (Adventure)resource.getContents().get(0);
		}

		if(object instanceof EObject){
			EObject o = (EObject)object;
			resource = (XMIResource)o.eResource();
			adventure = (Adventure)resource.getContents().get(0);
		}

		if(object instanceof EditPart){
			EditPart editpart = (EditPart)object;

			// will return a diagram
			object = editpart.getModel();
		}		


		// Active Geodiagram editor
		if(object instanceof Diagram){
			loadFromFile = true;
			
			Diagram diagram = (Diagram)object;
			adventure = (Adventure)diagram.getElement();
			resource = (XMIResource)adventure.eResource();	
		}
		if(object instanceof ViewImpl){
			ViewImpl shape = (ViewImpl)object;
			Diagram diagram = shape.getDiagram();
			adventure = (Adventure)diagram.getElement();
			resource = (XMIResource)adventure.eResource();		
		}
		
		return adventure;
	}
	
	/**
	 * Gets the path to a file which would be used for saving the transformation
	 * @param resource
	 * @param extension
	 * @return
	 */
	public java.net.URI getURIForFileToSave(XMIResource resource, String extension){
		return java.net.URI.create(resource.getURI().trimFileExtension().appendFileExtension(extension).toString());
	}
}
