package de.tu_bs.isf.mbse.textadventure.transformation.epsilon;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.epsilon.common.parse.problem.ParseProblem;
import org.eclipse.epsilon.egl.EglTemplateFactory;
import org.eclipse.epsilon.egl.EglTemplateFactoryModuleAdapter;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.emc.plainxml.StringInputStream;
import org.eclipse.epsilon.eol.dt.ExtensionPointToolNativeTypeDelegate;

import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;

/**
 * Converts an Adventure model to a game description via EGL.
 * Use this class to call the transformation via Java.
 * @author Gil Engel
 *
 */
public class EGLStandalone extends EpsilonStandalone {
	public EGLStandalone(URI eglFile) throws Exception{
		module = new EglTemplateFactoryModuleAdapter(new EglTemplateFactory());
		module.parse(eglFile);

	    if (module.getParseProblems().size() > 0) {
	        System.err.println("Parse errors occured...");
	        for (ParseProblem problem : module.getParseProblems()) {
	          System.err.println(problem.toString());
	        }
	        return;
	      }
	    
	    module.getContext().getNativeTypeDelegates().add(new ExtensionPointToolNativeTypeDelegate());
	}
	
	/**
	 * Transform a Adventure model instance to a html output that contains a description of the game
	 * @param Instance of Adventure
	 * @param URI of the file where the output should be saved
	 * @throws Exception
	 */
	public void transformToGameDescription(Adventure adventure, URI savegameFileURI) throws Exception{
		EmfModel emfModel = createEmfModel(adventure); 

		module.getContext().getModelRepository().addModel(emfModel);
		Object result = module.execute();
		
		String htmlStr = result.toString();
		saveToFile(savegameFileURI, htmlStr);
		
		module.reset();
		
	}	
	
	public void transformToGameDescription(URI adventure, URI metamodel, URI savegameFileURI) throws Exception{
		EmfModel emfModel = createEmfModelByURI("Adventure", adventure, "", true, false);

		module.getContext().getModelRepository().addModel(emfModel);
		Object result = module.execute();
		
		String htmlStr = result.toString();
		saveToFile(savegameFileURI, htmlStr);
		
		module.reset();		
	}

	private void saveToFile(URI savegameFileURI, String htmlStr) throws CoreException, UnsupportedEncodingException {
		IFile outFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(org.eclipse.emf.common.util.URI.createURI(savegameFileURI.toString()).toPlatformString(true)));
		if (!outFile.exists()) {
			outFile.create(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")), true, null);
			outFile.setCharset("UTF-8", null);
		} else {
			outFile.setContents(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")), true, false, null);
		}
	}
	
}
