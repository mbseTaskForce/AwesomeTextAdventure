package de.tu_bs.isf.mbse.textadventure.transformation.util;

import java.awt.Dimension;
import java.util.ArrayList;

import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;
import de.tu_bs.isf.mbse.textadventure.adventure.AndCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.Condition;
import de.tu_bs.isf.mbse.textadventure.adventure.DecorativeItem;
import de.tu_bs.isf.mbse.textadventure.adventure.Direction;
import de.tu_bs.isf.mbse.textadventure.adventure.Door;
import de.tu_bs.isf.mbse.textadventure.adventure.HasExaminedItemCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.HasItemCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.HasUsedItemCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.HasVisitedRoomCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.IsInRoomCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.Item;
import de.tu_bs.isf.mbse.textadventure.adventure.LeafCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.NotCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.OrCondition;
import de.tu_bs.isf.mbse.textadventure.adventure.Room;

/**
 * Helper class for EGL Transformation
 * @author Gil Engel
 *
 */
public class Adventure2TextUtil {
	private int translateX;
	private int translateY = -200;
	
	private Dimension size;
	
	public int getWidth(){
		return size.width;
	}
	public int getHeight(){
		return size.height;
	}
	public Room getRoomWithDoor(Adventure adventure, Door door){
		for(Room room : adventure.getRooms()){
			if(room.getDoors().contains(door))
				return room;
		}
		
		return null;
	}
	public String drawMap(Adventure adventure, ArrayList<Room> visited, int x, int y, Room room){
		boolean startRoom = adventure.getStartRoom().equals(room);
		String string = "<g>";
		string +=   "<a xlink:href=\"#"+room.getId()+"\" xlink:title=\""+room.getName()+"\"><rect x=\""+(x+1)+"\" y=\""+(y+11)+"\" width=\"200\" height=\"200\" class=\""+(startRoom ? "startRoom" : "room")+"\" /></a>\n";
		string += "<text class=\"description\" x=\""+(x+15)+"\" y=\""+(y+36)+"\">"+room.getName()+"</text>";
		
		Room neighbouringRoom;
		for(Door door : room.getDoors()){
			Door otherDoor;
			if(door.equals(door.getConnection().getDoors().get(0))){
				otherDoor = door.getConnection().getDoors().get(1);
			}else{
				otherDoor = door.getConnection().getDoors().get(0);
			}
			
			if(door.getDirection() == Direction.NORTH){
				string += "<rect x=\""+(x+80)+"\" y=\""+(y+1)+"\" width=\"40\" height=\"20\" class=\"door\"/>";
				neighbouringRoom = getRoomWithDoor(adventure, otherDoor);
				if(!visited.contains(neighbouringRoom)){
					translateY+=200;
					visited.add(neighbouringRoom);
					string += drawMap(adventure, visited, x, y - 200, neighbouringRoom);
					
					size.height += 200;
				}
			}else
			if(door.getDirection() == Direction.EAST){
				string += "<rect x=\""+(x+190)+"\" y=\""+(y+90)+"\" width=\"20\" height=\"40\" class=\"door\"/>";
				
				neighbouringRoom = getRoomWithDoor(adventure, otherDoor);
				if(!visited.contains(neighbouringRoom)){
					visited.add(neighbouringRoom);
					string += drawMap(adventure, visited, x+200, y, neighbouringRoom);
					
					size.width += 200;
				}					
			}else
			if(door.getDirection() == Direction.SOUTH){
				string += "<rect x=\""+(x+80)+"\" y=\""+(y+200)+"\" width=\"40\" height=\"20\" class=\"door\"/>";
				
				neighbouringRoom = getRoomWithDoor(adventure, otherDoor);
				if(!visited.contains(neighbouringRoom)){
					visited.add(neighbouringRoom);
					string += drawMap(adventure, visited, x, y + 200, neighbouringRoom);
					
					size.height += 200;
				}				
			}else
			if(door.getDirection() == Direction.WEST){
				string += "<rect x=\""+(x-10)+"\" y=\""+(y+90)+"\" width=\"20\" height=\"40\" class=\"door\"/>";

				neighbouringRoom = getRoomWithDoor(adventure, otherDoor);
				if(!visited.contains(neighbouringRoom)){
					translateX+=200;
					visited.add(neighbouringRoom);
					string += drawMap(adventure, visited, x-200, y, neighbouringRoom);
					
					size.width += 200;
				}			
			}			
		}
		
		string += "</g>";
		
		return string;
	}

	/**
	 * Returns a string which contains a svg map
	 * @param adventure
	 * @param startRoom
	 * @return String with svg map to embed it in a html file
	 */
	public String drawMap(Adventure adventure, Room startRoom){
		size = new Dimension();
		
		ArrayList<Room> rooms = new ArrayList<Room>();
		
		String map = drawMap(adventure, rooms, 0, 0 , startRoom);
		return "<g transform=\"translate("+((float)translateX)+","+(translateY)+")\">"+map+"</g>";
	}
	
	/**
	 * Returns a list (not a html structured one) with all conditions that are related
	 * @param string
	 * @param condition
	 * @return
	 */
	public String getConditionString(String string, Condition condition){
		if(condition instanceof AndCondition){
			string += getConditionString(string, ((AndCondition)condition).getLeftOperand()) +
					" <b>and</b> " + getConditionString(string, ((AndCondition)condition).getRightOperand());
		}else if(condition instanceof OrCondition){
			string += getConditionString(string, ((OrCondition)condition).getLeftOperand()) +
					" <b>or</b> " + getConditionString(string, ((OrCondition)condition).getRightOperand());			
		}else if(condition instanceof NotCondition){
			string += "<b>not</b> "+getConditionString(string, ((NotCondition)condition).getOperand());
		}else{
			if(condition instanceof HasVisitedRoomCondition){
				Room room = ((HasVisitedRoomCondition)condition).getRoom();
				return "you've visited <a href=\"#"+room.getId()+"\">"+room.getName()+"</a>";
			}else 
			if(condition instanceof IsInRoomCondition){
				Room room = ((IsInRoomCondition)condition).getRoom();
				return "you are in room <a href=\"#"+room.getId()+"\">"+room.getName()+"</a>";				
			}else 
			if(condition instanceof HasExaminedItemCondition){
				Item item = ((HasExaminedItemCondition)condition).getItem();
				return "you've examined the item <a href=\"#"+item.getId()+"\">"+item.getName()+"</a>";
			}else 
			if(condition instanceof HasUsedItemCondition){
				Item item = ((HasUsedItemCondition)condition).getItem();
				return "you've used the item <a href=\"#"+item.getId()+"\">"+item.getName()+"</a>";
			}else 
			if(condition instanceof HasItemCondition){
				Item item = ((HasItemCondition)condition).getItem();
				return "you have the item <a href=\"#"+item.getId()+"\">"+item.getName()+"</a>";
			}			
		}
		
		return string;
	}
	public String getNestedConditionString(Condition condition){
		String winningCondition = "";
		
			winningCondition = getConditionString(winningCondition, condition);
		
		
		return winningCondition;
	}
	
	public boolean isAndCondition(Condition condition){
		return condition instanceof AndCondition;
	}
	public boolean isOrCondition(Condition condition){
		return condition instanceof OrCondition;
	}
	public boolean isNotCondition(Condition condition){
		return condition instanceof NotCondition;
	}
	public boolean isLeafCondition(Condition condition){
		return condition instanceof LeafCondition;
	}	
	public boolean isHasExaminedItemCondition(Condition condition){
		return condition instanceof HasExaminedItemCondition;
	}	
	public boolean isHasUsedItemCondition(Condition condition){
		return condition instanceof HasUsedItemCondition;
	}	
	public boolean isIsInRoomCondition(Condition condition){
		return condition instanceof IsInRoomCondition;
	}	
	public boolean isHasVisitedRoomCondition(Condition condition){
		return condition instanceof LeafCondition;
	}	
	public boolean isDecorativeItem(Item item){
		return item instanceof DecorativeItem;
	}
}
