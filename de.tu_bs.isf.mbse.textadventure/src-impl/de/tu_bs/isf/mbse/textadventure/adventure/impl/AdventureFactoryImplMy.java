package de.tu_bs.isf.mbse.textadventure.adventure.impl;

import java.util.UUID;

import de.tu_bs.isf.mbse.textadventure.adventure.DecorativeItem;
import de.tu_bs.isf.mbse.textadventure.adventure.Door;
import de.tu_bs.isf.mbse.textadventure.adventure.DoorConnection;
import de.tu_bs.isf.mbse.textadventure.adventure.InventoryItem;
import de.tu_bs.isf.mbse.textadventure.adventure.Room;

public class AdventureFactoryImplMy extends AdventureFactoryImpl{
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		room.setId(UUID.randomUUID().toString());
		return room;
	}
	
	public DecorativeItem createDecorativeItem() {
		DecorativeItemImpl decorativeItem = new DecorativeItemImpl();
		decorativeItem.setId(UUID.randomUUID().toString());
		return decorativeItem;
	}
	
	public InventoryItem createInventoryItem() {
		InventoryItemImpl inventoryItem = new InventoryItemImpl();
		inventoryItem.setId(UUID.randomUUID().toString());
		return inventoryItem;
	}	
	
	public Door createDoor() {
		DoorImpl door = new DoorImpl();
		door.setId(UUID.randomUUID().toString());
		return door;
	}
	
	public DoorConnection createDoorConnection() {
		DoorConnectionImpl doorConnection = new DoorConnectionImplMy();
		doorConnection.setId(UUID.randomUUID().toString());
		return doorConnection;
	}
}
