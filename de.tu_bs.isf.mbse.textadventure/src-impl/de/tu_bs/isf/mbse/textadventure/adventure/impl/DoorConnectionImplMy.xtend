package de.tu_bs.isf.mbse.textadventure.adventure.impl

import de.tu_bs.isf.mbse.textadventure.adventure.Door

class DoorConnectionImplMy extends DoorConnectionImpl {
	override basicGetDoor1() {
		if (getDoors().size > 0) {
			getDoors().get(0)
		} else {
			null
		}
	}
	
	override setDoor1(Door newDoor1) {
		val oldDoor1 = getDoor1()
		if (oldDoor1 != null) {
			getDoors().remove(oldDoor1)
		}
		getDoors().add(newDoor1)
	}
	
	override basicGetDoor2() {
		if (getDoors().size > 1) {
			getDoors().get(1)
		} else {
			null
		}
	}
	
	override setDoor2(Door newDoor2) {
		val oldDoor2 = getDoor2()
		if (oldDoor2 != null) {
			getDoors().remove(oldDoor2)
		}
		getDoors().add(newDoor2)
	}
}