package de.tu_bs.isf.mbse.textadventure.savegame.impl;

import de.tu_bs.isf.mbse.textadventure.savegame.State;

public class SavegameFactoryImplMy extends SavegameFactoryImpl {
	public State createState() {
		StateImpl state = new StateImplMy();
		return state;
	}
}
