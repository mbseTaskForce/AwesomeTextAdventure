package de.tu_bs.isf.mbse.textadventure.savegame.util

import de.tu_bs.isf.mbse.textadventure.adventure.AndCondition
import de.tu_bs.isf.mbse.textadventure.adventure.HasExaminedItemCondition
import de.tu_bs.isf.mbse.textadventure.adventure.HasItemCondition
import de.tu_bs.isf.mbse.textadventure.adventure.HasUsedItemCondition
import de.tu_bs.isf.mbse.textadventure.adventure.HasVisitedRoomCondition
import de.tu_bs.isf.mbse.textadventure.adventure.IsInRoomCondition
import de.tu_bs.isf.mbse.textadventure.adventure.NotCondition
import de.tu_bs.isf.mbse.textadventure.adventure.OrCondition
import de.tu_bs.isf.mbse.textadventure.savegame.State

class ConditionChecker {
	val public static INSTANCE = new ConditionChecker()
	
	private new() {
		
	}
	
	def dispatch boolean check(Void condition, State state) {
		true
	}
	
	def dispatch boolean check(NotCondition condition, State state) {
		!(condition.operand.check(state))
	}
	
	def dispatch boolean check(AndCondition condition, State state) {
		condition.leftOperand.check(state) && condition.rightOperand.check(state) 
	}
	
	def dispatch boolean check(OrCondition condition, State state) {
		condition.leftOperand.check(state) || condition.rightOperand.check(state) 
	}
	
	def dispatch boolean check(HasItemCondition condition, State state) {
		state.playerInventory.contains(condition.item)
	}
	
	def dispatch boolean check(HasUsedItemCondition condition, State state) {
		state.usedItems.contains(condition.item)
	}
	
	def dispatch boolean check(HasExaminedItemCondition condition, State state) {
		state.examinedItems.contains(condition.item)
	}
	
	def dispatch boolean check(IsInRoomCondition condition, State state) {
		state.currentRoom == condition.room
	}
	
	def dispatch boolean check(HasVisitedRoomCondition condition, State state) {
		state.visitedRooms.contains(condition.room)
	}
}