package de.tu_bs.isf.mbse.textadventure.savegame.impl

import de.tu_bs.isf.mbse.textadventure.adventure.Condition
import de.tu_bs.isf.mbse.textadventure.adventure.IDescribedElement
import de.tu_bs.isf.mbse.textadventure.adventure.Message
import de.tu_bs.isf.mbse.textadventure.savegame.util.ConditionChecker
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList

class StateImplMy extends StateImpl {
	override getVisibleItems() {
		currentRoomInventory.items
			.filter[this.satisfiesCondition(visibilityCondition)]
			.toEList
	}
	
	override getCurrentRoomInventory() {
		getRoomInventories().findFirst[room == currentRoom]
	}
	
	override satisfiesCondition(Condition condition) {
		ConditionChecker.INSTANCE.check(condition, this)
	}
	
	override getCurrentDescription(IDescribedElement describedElement) {
		val messages = describedElement.messages.filter[this.satisfiesCondition(condition)]
		
		describedElement.description + " " + (getCurrentMessages(messages.toEList) ?: "")
	}
	
	override getCurrentMessages(EList<Message> messages) {
		val messageContents = messages?.map[content]
		
		if (messageContents == null || messageContents.empty) {
			null
		} else {
			messageContents.join(" ")
		}
	}
	
	/**
	 * Ecore can't describe API that accepts iterables, so this clutch is needed :(
	 */
	def private <T> toEList(Iterable<T> iterable) {
		new BasicEList(iterable.toList)
	}
}