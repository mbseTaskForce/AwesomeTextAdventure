package de.tu_bs.isf.mbse.textadventure.xtext;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.shared.SharedStateModule;
import org.osgi.framework.BundleContext;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.util.Modules;

import de.tu_bs.isf.mbse.textadventure.xtext.adventure.AdventureRuntimeModule;
import de.tu_bs.isf.mbse.textadventure.xtext.adventure.ui.AdventureUiModule;

/**
 * This plugin interfaces the textadventure models with Xtext
 * 
 * As per https://christiandietrich.wordpress.com/2011/07/17/xtext-2-0-and-uml/
 * 
 * @author tilman
 *
 */
public class Activator extends AbstractUIPlugin {
	private static Activator plugin;

	private Injector adventureInjector;
	// private Injector savegameInjector;

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		initializeInjector();
	}

	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}
	
	public Injector getAdventureInjector() {
		return adventureInjector;
	}

	private void initializeInjector() {
		adventureInjector = Guice.createInjector(
				Modules.override(Modules.override(new AdventureRuntimeModule())
				.with(new AdventureUiModule(plugin)))
				.with(new SharedStateModule()));
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
}
