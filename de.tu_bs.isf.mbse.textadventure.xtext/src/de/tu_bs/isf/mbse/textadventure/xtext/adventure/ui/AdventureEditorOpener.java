package de.tu_bs.isf.mbse.textadventure.xtext.adventure.ui;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.ui.IEditorPart;
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener;

public class AdventureEditorOpener extends LanguageSpecificURIEditorOpener {
    @Override
    protected void selectAndReveal(IEditorPart openEditor, URI uri,
            EReference crossReference, int indexInList, boolean select) {
    	// TODO interface with graphical editor
    	super.selectAndReveal(openEditor, uri, crossReference, indexInList, select);
    }
}
