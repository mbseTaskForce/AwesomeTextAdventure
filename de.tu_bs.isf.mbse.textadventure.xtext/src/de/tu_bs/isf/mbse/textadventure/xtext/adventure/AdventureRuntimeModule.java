package de.tu_bs.isf.mbse.textadventure.xtext.adventure;

import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.SimpleNameProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;

public class AdventureRuntimeModule extends AbstractGenericResourceRuntimeModule {

	@Override
	protected String getLanguageName() {
		return "de.tu_bs.isf.mbse.textadventure.xtext.adventure.Adventure";
	}

	@Override
	protected String getFileExtensions() {
		return "adventure";
	}
	
    public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
        return DefaultResourceDescriptionStrategy.class;
    }
 
    @Override
    public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
    	// TODO implement own QualifiedNameProvider
        return SimpleNameProvider.class;
    }
}
