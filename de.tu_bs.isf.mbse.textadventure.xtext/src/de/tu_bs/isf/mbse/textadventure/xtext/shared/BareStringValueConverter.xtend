package de.tu_bs.isf.mbse.textadventure.xtext.shared

import org.eclipse.xtext.conversion.impl.QualifiedNameValueConverter

class BareStringValueConverter extends QualifiedNameValueConverter {
		override getNamespaceDelimiter() {
			" "
		}
	}