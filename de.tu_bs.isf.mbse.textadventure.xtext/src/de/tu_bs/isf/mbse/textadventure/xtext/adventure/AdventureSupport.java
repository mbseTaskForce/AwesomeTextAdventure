package de.tu_bs.isf.mbse.textadventure.xtext.adventure;

import org.eclipse.xtext.resource.generic.AbstractGenericResourceSupport;

import com.google.inject.Module;
 
public class AdventureSupport extends AbstractGenericResourceSupport {
 
    @Override
    protected Module createGuiceModule() {
        return new AdventureRuntimeModule();
    }
 
}
