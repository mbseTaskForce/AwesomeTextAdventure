
/*
 * 
 */
package de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers;

import java.text.FieldPosition;
import java.text.MessageFormat;
import java.text.ParsePosition;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.tooling.runtime.parsers.AbstractAttributeParser;
import org.eclipse.osgi.util.NLS;


public class TypeParser extends MessageFormatParser {


	public TypeParser(EAttribute[] features) {
		super(features);
	}


	public TypeParser(EAttribute[] features, EAttribute[] editableFeatures) {
		super(features, editableFeatures);
	}


	public String getPrintString(IAdaptable adapter, int flags) {
		EObject element = (EObject) adapter.getAdapter(EObject.class);
		return element.eClass().getName();
	}

}
