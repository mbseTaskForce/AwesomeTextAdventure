/*
 * 
 */
package de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers.TypeParser;

public class AdventureParserProviderCustom extends AdventureParserProvider implements IParserProvider {

	private IParser decorativeItemName_5001Parser;

	private IParser getDecorativeItemName_5001Parser() {
		if (decorativeItemName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] {
					de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage.eINSTANCE.getINamedElement_Name() };
			de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers.MessageFormatParser parser = new de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers.MessageFormatParser(
					features);
			decorativeItemName_5001Parser = parser;
		}
		return decorativeItemName_5001Parser;
	}


	private IParser inventoryItemName_5002Parser;


	private IParser getInventoryItemName_5002Parser() {
		if (inventoryItemName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] {
					de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage.eINSTANCE.getINamedElement_Name() };
			de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers.MessageFormatParser parser = new de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.parsers.MessageFormatParser(
					features);
			inventoryItemName_5002Parser = parser;
		}
		return inventoryItemName_5002Parser;
	}
	
	
	private IParser useActionName_5003Parser;
	
	
	private IParser getUseActionName_5003Parser() {
		if (useActionName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] {
					de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage.eINSTANCE.getINamedElement_Name() };
			TypeParser parser = new TypeParser(
					features);
			useActionName_5003Parser = parser;
		}
		return useActionName_5003Parser;
	}


	private IParser pickUpActionName_5004Parser;


	private IParser getPickUpActionName_5004Parser() {
		if (pickUpActionName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] {
					de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage.eINSTANCE.getINamedElement_Name() };
			TypeParser parser = new TypeParser(
					features);
			pickUpActionName_5004Parser = parser;
		}
		return pickUpActionName_5004Parser;
	}


	private IParser dropActionName_5005Parser;


	private IParser getDropActionName_5005Parser() {
		if (dropActionName_5005Parser == null) {
			EAttribute[] features = new EAttribute[] {
					de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage.eINSTANCE.getINamedElement_Name() };
			TypeParser parser = new TypeParser(
					features);
			dropActionName_5005Parser = parser;
		}
		return dropActionName_5005Parser;
	}
	
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.edit.parts.DecorativeItemNameEditPart.VISUAL_ID:
			return getDecorativeItemName_5001Parser();
		case de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.edit.parts.InventoryItemNameEditPart.VISUAL_ID:
			return getInventoryItemName_5002Parser();
		case de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.edit.parts.UseActionNameEditPart.VISUAL_ID:
			return getUseActionName_5003Parser();
		case de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.edit.parts.PickUpActionNameEditPart.VISUAL_ID:
			return getPickUpActionName_5004Parser();
		case de.tu_bs.isf.mbse.textadventure.adventure.itemdiagram.edit.parts.DropActionNameEditPart.VISUAL_ID:
			return getDropActionName_5005Parser();
		}
		return null;
	}

}
