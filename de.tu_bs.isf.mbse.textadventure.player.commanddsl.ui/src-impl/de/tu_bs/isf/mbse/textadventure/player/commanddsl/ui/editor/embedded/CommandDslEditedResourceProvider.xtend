package de.tu_bs.isf.mbse.textadventure.player.commanddsl.ui.editor.embedded

import com.google.inject.Inject
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.embedded.IEditedResourceProvider

class CommandDslEditedResourceProvider implements IEditedResourceProvider {
	@Accessors ResourceSet resourceSet
	@Inject FileExtensionProvider ext

	override createResource() {
		val uri = URI.createURI('''synthetic:/command.«ext.getPrimaryFileExtension()»''')
		return resourceSet.createResource(uri) as XtextResource
	}

}
