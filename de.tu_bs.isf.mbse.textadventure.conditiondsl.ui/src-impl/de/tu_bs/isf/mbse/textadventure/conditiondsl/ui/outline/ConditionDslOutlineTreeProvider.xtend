/*
 * generated by Xtext
 */
package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.outline

/**
 * Customization of the default outline structure.
 *
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#outline
 */
class ConditionDslOutlineTreeProvider extends org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider {
	
}
