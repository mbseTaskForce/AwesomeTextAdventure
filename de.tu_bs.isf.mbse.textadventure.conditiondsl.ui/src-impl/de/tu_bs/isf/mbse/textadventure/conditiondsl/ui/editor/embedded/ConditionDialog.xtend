package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded

import com.google.inject.Inject
import de.tu_bs.isf.mbse.textadventure.adventure.Condition
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.ConditionDslFactory
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.Root
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded.internal.DslDialog
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.internal.ConditionDIFacade
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.serializer.ISerializer

class ConditionDialog extends DslDialog {
	def static createInstance(Shell parentShell, ResourceSet resSet, Condition condition) {
		val injector = ConditionDIFacade.defaultInjector
		
		val dialog = new ConditionDialog(parentShell, resSet, condition)
		injector.injectMembers(dialog)
		
		return dialog
	}
	
	@Inject val ISerializer serializer = null
	
	val Condition condition

	@Inject
	protected new(Shell parentShell, ResourceSet editorResourceSet, Condition condition) {
		super(parentShell, editorResourceSet)
		this.condition = condition
	}

	def getCondition() {
		embeddedEditor.document.readOnly[res |
			(res.contents.get(0) as Root).condition
		]
	}
	
	override protected getPrefix() {
		"cond"
	}

	override protected getEditablePart() {
		if (condition == null) {
			return ""
		}
		
		val root = ConditionDslFactory.eINSTANCE.createRoot
		root.condition = EcoreUtil.copy(condition)
		
		root.containInResourceSet
		
		serializer.serialize(root.condition)
	}
	
	override protected getSuffix() {
		""
	}
}