package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded

import com.google.inject.Inject
import de.tu_bs.isf.mbse.textadventure.adventure.Message
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.ConditionDslFactory
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.Root
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded.internal.DslDialog
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.internal.ConditionDIFacade
import java.util.List
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.serializer.ISerializer

class MessagesDialog extends DslDialog {
	def static createInstance(Shell parentShell, ResourceSet resSet, List<Message> messages) {
		val injector = ConditionDIFacade.defaultInjector
		
		val dialog = new MessagesDialog(parentShell, resSet, messages)
		injector.injectMembers(dialog)
		
		return dialog
	}
	
	@Inject val ISerializer serializer = null
	
	val List<Message> messages

	@Inject
	protected new(Shell parentShell, ResourceSet editorResourceSet, List<Message> messages) {
		super(parentShell, editorResourceSet)
		this.messages = messages
	}

	def getMessages() {
		embeddedEditor.document.readOnly[res |
			(res.contents.get(0) as Root).messages
		]
	}
	
	override protected getPrefix() {
		"msg"
	}

	override protected getEditablePart() {
		if (messages == null || messages.isEmpty) {
			return ""
		}
		
		val root = ConditionDslFactory.eINSTANCE.createRoot
		root.messages += EcoreUtil.copyAll(messages)
		
		root.containInResourceSet
		
		root.messages.map[serializer.serialize(it)].join("\n")
	}
	
	override protected getSuffix() {
		""
	}
}