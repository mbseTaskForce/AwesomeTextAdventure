package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.internal

import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.internal.ConditionDslActivator

class ConditionDIFacade {
	def static getDefaultInjector() {
		 ConditionDslActivator.instance.getInjector(ConditionDslActivator.DE_TU_BS_ISF_MBSE_TEXTADVENTURE_CONDITIONDSL_CONDITIONDSL)
	}
}