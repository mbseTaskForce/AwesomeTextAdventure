package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.syntaxcoloring

import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration

class ConditionDslAntlrTokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {
	val static functionKeywords = #["'hasItem'", "'hasExaminedItem'", "'hasUsedItem'", "'isInRoom'", "'hasVisitedRoom'"]
	
	override calculateId(String tokenName, int tokenType) {
		if (functionKeywords.contains(tokenName)) {
			return 	DefaultHighlightingConfiguration.DEFAULT_ID
		}
		
		return super.calculateId(tokenName, tokenType)
	}
}