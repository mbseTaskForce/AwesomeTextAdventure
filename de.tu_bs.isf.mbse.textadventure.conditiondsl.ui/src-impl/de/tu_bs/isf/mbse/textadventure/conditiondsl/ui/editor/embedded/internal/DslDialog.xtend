package de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded.internal

import com.google.inject.Inject
import com.google.inject.Provider
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.Root
import java.util.Arrays
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.jface.dialogs.Dialog
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.jface.resource.JFaceResources
import org.eclipse.swt.graphics.Device
import org.eclipse.swt.graphics.Font
import org.eclipse.swt.graphics.Point
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Shell
import org.eclipse.xtext.resource.ResourceSetReferencingResourceSet
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorModelAccess
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator

abstract class DslDialog extends Dialog {
	val static FONT = JFaceResources.getFont(JFaceResources.TEXT_FONT)
	
	def static getFont(Device device) {
		val fd = Arrays.copyOf(FONT.fontData, FONT.fontData.length)
		// fd.get(0).height = 25
		new Font(device, fd)
	}
	
	@Inject val EmbeddedEditorFactory embeddedEditorFactory = null
	@Inject val Provider<ResourceSetReferencingResourceSet> resourcSetProvider = null
	@Inject val ConditionDslEditedResourceProvider resourceProvider = null
	@Inject val IResourceValidator resourceValidator = null
	
	val protected ResourceSet editorResourceSet
	
	var protected EmbeddedEditor embeddedEditor
	var protected EmbeddedEditorModelAccess modelAccess

	@Inject
	protected new(Shell parentShell, ResourceSet editorResourceSet) {
		super(parentShell)
		this.editorResourceSet = editorResourceSet
	}

	override createDialogArea(Composite parent) {
		val composite = super.createDialogArea(parent) as Composite
		
		val resSet = resourcSetProvider.get()
		resSet.referencedResourceSets.add(editorResourceSet)
		resourceProvider.resourceSet = resSet
		
		embeddedEditor = embeddedEditorFactory
			.newEditor(resourceProvider)
			.withResourceValidator(resourceValidator)
			.showErrorAndWarningAnnotations()
			.withParent(composite)
			
		embeddedEditor.viewer.textWidget.font = getFont(parent.display)
		
		modelAccess = embeddedEditor.createPartialEditor(
			getPrefix(), 
			getEditablePart(), 
			getSuffix(), 
			true
		)
		
		return composite
	}
	
	override isResizable() {
		true
	}
	
	override okPressed() {
		val issues = embeddedEditor.document.readOnly[res |
			resourceValidator.validate(res, CheckMode.ALL, null)
		] 
		if (issues.size > 0) {
			MessageDialog.openError(shell, "Invalid", "The input is not valid.")
			return
		}
		
		setReturnCode(OK)
		close()
	}
	
	override protected getInitialSize() {
		new Point(550, 250)
	}
	
	def protected abstract String getPrefix() 
	
	def protected abstract String getEditablePart()
	
	def protected abstract String getSuffix()
	
	def protected containInResourceSet(Root root) {
		val resSet = resourcSetProvider.get()
		resSet.referencedResourceSets.add(editorResourceSet)
		resourceProvider.resourceSet = resSet
		
		val res = resourceProvider.createResource
		res.contents.add(root)

	}
}