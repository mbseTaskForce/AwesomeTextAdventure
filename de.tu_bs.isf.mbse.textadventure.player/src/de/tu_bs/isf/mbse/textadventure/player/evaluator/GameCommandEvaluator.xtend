package de.tu_bs.isf.mbse.textadventure.player.evaluator

import de.tu_bs.isf.mbse.textadventure.adventure.Action
import de.tu_bs.isf.mbse.textadventure.adventure.Adventure
import de.tu_bs.isf.mbse.textadventure.adventure.Combination
import de.tu_bs.isf.mbse.textadventure.adventure.InventoryItem
import de.tu_bs.isf.mbse.textadventure.adventure.Room
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.CombineCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.DropCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.ExamineCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.GoCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.InventoryCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.PickUpCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.UseCommand
import de.tu_bs.isf.mbse.textadventure.savegame.State
import java.util.Collection
import java.util.List

class GameCommandEvaluator implements IGameCommandEvaluator {
	def dispatch evaluate(State state, PickUpCommand command) {
		val item = command.item
		
		if (item instanceof InventoryItem) {
			if (state.canPerformAction(item.pickUpAction, true)) {
				state.playerInventory.add(item)
				state.currentRoomInventory.items.remove(item)
				
				state.getCurrentMessages(item.pickUpAction?.allowedMessages) ?: '''You pick up «item.name»'''
			} else {
				state.getCurrentMessages(item.pickUpAction?.deniedMessages) ?: "Can't pick that up."
			}
		} else {
			// TODO custom message for DecorativeItems that can't be picked up
			"Can't pick that up."
		}
	}
	
	def dispatch evaluate(State state, DropCommand command) {
		val item = command.item
		
		if (state.canPerformAction(item.dropAction, false)) {
			state.playerInventory.remove(item)
			state.currentRoomInventory.items.add(item)
			
			state.getCurrentMessages(item.dropAction?.allowedMessages) ?: '''You drop up «item.name»'''
		} else {
			state.getCurrentMessages(item.dropAction?.deniedMessages) ?: "I might still need this."
		}
	}
	
	def dispatch evaluate(State state, UseCommand command) {
		val item = command.item
		
		if (state.canPerformAction(item.useAction, false)) {
			state.usedItems.add(item)
			
			state.getCurrentMessages(item.useAction?.allowedMessages) ?: '''You use «item.name»'''
		} else {
			state.getCurrentMessages(item.useAction?.deniedMessages) ?: "I don't know what to do with that."
		}
	}
	
	def dispatch evaluate(State state, CombineCommand command) {
		val inputItems = command.items
		
		val combinations = state.game.getMatchingCombinations(inputItems)
		
		switch (combinations.size) {
			case 0: "I can't combine that."
			case 1: {
				val combination = combinations.head
				if (state.satisfiesCondition(combination.condition)) {
					state.performCombination(combination)
				} else {
					// TODO custom failure message
					"I can't do that."
				}
			}
			default: {
				// We get here if there are two possible combinations. This should be forbidden by validation 
				// of the adventure model.
				throw new IllegalStateException("Multiple applicable combinations!")
			}
		}
	}
	
	def dispatch evaluate(State state, InventoryCommand command) {
		state.playerInventory.map['''«name»: «state.getCurrentDescription(it)»'''].join("\n")
	}
	
	def dispatch evaluate(State state, GoCommand command) {
		val doors = state.currentRoom.doors.filter[direction == command.direction]
		
		switch (doors.size) {
			case 0: '''Can't go «command.direction.toString»'''
			case 1: {
				val door = doors.head
				
				if (state.canPerformAction(door.enterAction, true)) {
					// TODO steh_ti make beautiful
					val connection = door.connection
					val targetRoom = (if (connection.door1.eContainer == state.currentRoom) {
						connection.door2.eContainer
					} else {
						connection.door1.eContainer
					}) as Room
					
					state.currentRoom = targetRoom
					
					val enterMsg = state.getCurrentMessages(door.enterAction?.allowedMessages) ?: '''You enter «targetRoom.name»'''
					'''«enterMsg»: «state.getCurrentDescription(targetRoom)» «targetRoom.readDirections»'''
				} else {
					state.getCurrentMessages(door.enterAction?.deniedMessages) ?: "Can't enter..."
				}
 				
				
			}
			default: throw new IllegalStateException("Multiple applicable connections!")
		}
	}
	
	def dispatch evaluate(State state, ExamineCommand command) {
		if (command.item == null) {
			val room = state.currentRoom
			return '''You are in «room.name»: «state.getCurrentDescription(room)» «room.readDirections»'''
		} else {
			state.examinedItems += command.item
			return state.getCurrentDescription(command.item)
		} 
	}

	
	def private String readDirections(Room room) {
		'''You can go «room.doors.map[direction.toString].toList.joinWithAnd()».'''
	}
	
		
	def private getMatchingCombinations(Adventure game, Collection<InventoryItem> inputItems) {
		game.combinations.filter[inputs.containsAll(inputItems) && inputs.size == inputItems.size]
	}
	
	def private String performCombination(State state, Combination combination) {
		state.playerInventory.removeAll(combination.inputs)
		state.playerInventory.addAll(combination.outputs)
		'''«state.getCurrentDescription(combination)» You now have «combination.outputs.map[name].joinWithAnd()».'''
	}
	
	def private canPerformAction(State state, Action action, Boolean defaultAllowed) {
		(action != null && state.satisfiesCondition(action.conditon)) 
			|| (action == null && defaultAllowed)
	}
	
	def private joinWithAnd(List<String> items) {
		if (items.size == 1) {
			items.head
		} else {
			items.subList(0, items.size - 1).join(", ") + " and " + items.last
		}
	}
}
