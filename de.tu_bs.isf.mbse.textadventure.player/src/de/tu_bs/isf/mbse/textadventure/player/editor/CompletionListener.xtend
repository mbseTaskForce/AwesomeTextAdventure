package de.tu_bs.isf.mbse.textadventure.player.editor

import org.eclipse.jface.text.contentassist.ContentAssistEvent
import org.eclipse.jface.text.contentassist.ICompletionListener
import org.eclipse.jface.text.contentassist.ICompletionListenerExtension
import org.eclipse.jface.text.contentassist.ICompletionProposal

class CompletionListener implements ICompletionListener, ICompletionListenerExtension {
		public static val DELAY = 50
		 
		var long lastAssistSessionEvent 
		var boolean assistSessionRunning = false
		
		def boolean isProposalPopupActive() {
			assistSessionRunning || (System.currentTimeMillis - lastAssistSessionEvent < DELAY)
		}
		
		override assistSessionStarted(ContentAssistEvent event) {
			assistSessionRunning = true
			lastAssistSessionEvent = System.currentTimeMillis()
		}
		
		override assistSessionEnded(ContentAssistEvent event) {
			assistSessionRunning = false
			lastAssistSessionEvent = System.currentTimeMillis()
		}
		
		override assistSessionRestarted(ContentAssistEvent event) {
			assistSessionRunning = true
			lastAssistSessionEvent = System.currentTimeMillis()
		}
		
		override selectionChanged(ICompletionProposal proposal, boolean smartToggle) {
			// not interesting
		}
		
	}