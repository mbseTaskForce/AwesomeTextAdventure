package de.tu_bs.isf.mbse.textadventure.player.editor

import com.google.inject.Inject
import de.tu_bs.isf.mbse.textadventure.adventure.provider.AdventureItemProviderAdapterFactory
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.CommandDocument
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.GameCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.SaveCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.ui.editor.embedded.CommandDslEditedResourceProvider
import de.tu_bs.isf.mbse.textadventure.player.evaluator.IGameCommandEvaluator
import de.tu_bs.isf.mbse.textadventure.savegame.State
import de.tu_bs.isf.mbse.textadventure.savegame.provider.SavegameItemProviderAdapterFactory
import java.util.Arrays
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.command.BasicCommandStack
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.edit.command.ChangeCommand
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain
import org.eclipse.emf.edit.provider.ComposedAdapterFactory
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory
import org.eclipse.emf.edit.ui.util.EditUIUtil
import org.eclipse.jface.dialogs.ProgressMonitorDialog
import org.eclipse.jface.resource.JFaceResources
import org.eclipse.jface.text.contentassist.ContentAssistant
import org.eclipse.swt.SWT
import org.eclipse.swt.custom.SashForm
import org.eclipse.swt.custom.VerifyKeyListener
import org.eclipse.swt.events.VerifyEvent
import org.eclipse.swt.graphics.Device
import org.eclipse.swt.graphics.Font
import org.eclipse.swt.layout.FillLayout
import org.eclipse.swt.widgets.Composite
import org.eclipse.swt.widgets.Text
import org.eclipse.ui.IEditorInput
import org.eclipse.ui.IEditorPart
import org.eclipse.ui.IEditorSite
import org.eclipse.ui.PartInitException
import org.eclipse.ui.part.EditorPart
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorModelAccess
import org.eclipse.xtext.ui.resource.IResourceSetProvider
import org.eclipse.xtext.validation.CheckMode
import org.eclipse.xtext.validation.IResourceValidator

class SavegameConsoleEditor extends EditorPart {
	val static FONT = JFaceResources.getFont(JFaceResources.TEXT_FONT)
	
	def static getFont(Device device) {
		val fd = Arrays.copyOf(FONT.fontData, FONT.fontData.length)
		// fd.get(0).height = 25
		new Font(device, fd)
	}
	
	@Inject val EmbeddedEditorFactory factory = null
	@Inject val CommandDslEditedResourceProvider resourceProvider = null
	@Inject val IResourceSetProvider resourceSetProvider = null
	@Inject val IResourceValidator validator = null
	
	@Inject val IGameCommandEvaluator commandEvaluator = null
	
	Text text
	
	EmbeddedEditor embeddedEditor
	EmbeddedEditorModelAccess modelAccess
	CompletionListener completionListener

	ComposedAdapterFactory adapterFactory
	AdapterFactoryEditingDomain editingDomain
	State savegame
	
	Resource savegameResource
	
	override void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site)
		setInput(input)
		partName = input.name
	}

	override void createPartControl(Composite parent) {
		initializeEditingDomain()
		createModel()
		doCreatePartControl(parent)
		checkGameEnd()
	}
	
	def doCreatePartControl(Composite parent) {
		parent.setLayout(new FillLayout(SWT.HORIZONTAL))
		
		var sashForm = new SashForm(parent, SWT.VERTICAL)
		
		text = new Text(
			sashForm, 
			SWT.BORDER
				.bitwiseOr(SWT.WRAP)
				.bitwiseOr(SWT.MULTI)
				.bitwiseOr(SWT.V_SCROLL)
				.bitwiseOr(SWT.READ_ONLY)
		)
		
		text.font = getFont(parent.display)
		
		text.text = '''
			Welcome to «savegame.game.name»
			------------------------------------------
			«savegame.getCurrentDescription(savegame.game)»
			
		'''
		
		createEmbeddedEditor(sashForm)
		
		sashForm.setWeights(#[5, 1])
	}

	override void setFocus() {
		embeddedEditor.viewer.textWidget.setFocus()
	}

	override void doSave(IProgressMonitor monitor) {
		val saveOptions = newHashMap(
			Resource.OPTION_SAVE_ONLY_IF_CHANGED -> Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER,
			Resource.OPTION_LINE_DELIMITER -> Resource.OPTION_LINE_DELIMITER_UNSPECIFIED
		)	
		
		new ProgressMonitorDialog(site.shell).run(true, false)[
			savegameResource.save(saveOptions)
		]
		
		(editingDomain.commandStack as BasicCommandStack).saveIsDone()
		firePropertyChange(IEditorPart.PROP_DIRTY)
	}

	override boolean isDirty() {
		(editingDomain.commandStack as BasicCommandStack).saveNeeded
	}

	override void doSaveAs() {
		throw new UnsupportedOperationException
	}

	override boolean isSaveAsAllowed() {
		false
	}
	
	def private initializeEditingDomain() {
		adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE)

		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory)
		adapterFactory.addAdapterFactory(new AdventureItemProviderAdapterFactory)
		adapterFactory.addAdapterFactory(new SavegameItemProviderAdapterFactory)
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory)
		
		val commandStack = new BasicCommandStack()
		commandStack.addCommandStackListener[event |
			firePropertyChange(IEditorPart.PROP_DIRTY)
		]
		// obtain the resource set via CommandDsls IResourceSetProvider so we get an XtextResourceSet which the editor can use
		var resourceSet = resourceSetProvider.get(null)
		editingDomain = new AdapterFactoryEditingDomain(adapterFactory, commandStack, resourceSet);
	}
	
	def private createModel() {
		val resourceURI = EditUIUtil.getURI(editorInput, editingDomain.resourceSet.URIConverter)
		savegameResource = editingDomain.resourceSet.getResource(resourceURI, true)
		savegame = savegameResource.contents.get(0) as State

		editingDomain.resourceToReadOnlyMap = newHashMap(
			savegameResource -> false,
			savegame.game.eResource -> true
		)
	}

	def private createEmbeddedEditor(Composite parent) {
		resourceProvider.resourceSet = editingDomain.resourceSet
		
		embeddedEditor = factory
			.newEditor(resourceProvider)
			.showErrorAndWarningAnnotations
			.withParent(parent)
			
		embeddedEditor.viewer.textWidget.font = getFont(parent.display)
			
		modelAccess = embeddedEditor.createPartialEditor(createInputPrefix(), "", "", false)
		embeddedEditor.viewer.textWidget.addVerifyKeyListener(new InputKeyListener(this))
		
		completionListener = new CompletionListener()
		(embeddedEditor.viewer.contentAssistant as ContentAssistant).addCompletionListener(completionListener)
		
	}
	
	def private URI findAdventureURI() {
		savegame.game.eResource.URI
	}
	
	def private createInputPrefix() {
		'''import "«findAdventureURI()»"'''.toString + "\n"
	}
	
	def private evaluateDocument() {
		val document = embeddedEditor.document
		document.readOnly[resource | 
			val commandStr = modelAccess.editablePart.trim()
			text.append("> " + commandStr + "\n")
			
			val issues = validator.validate(resource, CheckMode.ALL, null)
			if (issues.isEmpty) {
				val command = (resource.contents.get(0) as CommandDocument).command
				val result = command.evaluate
				text.append(result.trim() + "\n\n")
				
				checkGameEnd()
				
				modelAccess.updateModel(createInputPrefix(), "", "")
			} else if (commandStr.length > 0) {
				val issueStrs = issues.map[message]
				text.append("Errors:\n" + issueStrs.join("\n") + "\n\n")
			}
			
			null
		]
	}
	
	def private dispatch evaluate(GameCommand command) {
		var emfCommand = new GameCommandCommand(this, command)
		editingDomain.commandStack.execute(emfCommand)
		return emfCommand.returned
	}
	
	def private dispatch evaluate(SaveCommand command) {
		doSave(new NullProgressMonitor)
		return "Game saved!"
	}	
	
	def private checkGameEnd() {
		if (savegame.isWon) {
			text.append('''You have won «savegame.game.name»!''')
			embeddedEditor.viewer.textWidget.enabled = false
		} else if (savegame.isFailed) {
			text.append('''You have lost «savegame.game.name»!''')
			embeddedEditor.viewer.textWidget.enabled = false
		}
	}
	
	def private isWon(State state) {
		state.satisfiesCondition(state.game.winCondition)
	}
	
	def private isFailed(State state) {
		state.satisfiesCondition(state.game.failCondition)
	}
	
	private static class InputKeyListener implements VerifyKeyListener {
		SavegameConsoleEditor editor
		
		new(SavegameConsoleEditor editor) {
			this.editor = editor
		}
			
		override verifyKey(VerifyEvent e) {
	    	if (editor.completionListener.proposalPopupActive) {
	    		return
	    	}
	    	
			if (e.keyCode == SWT.CR && e.stateMask.bitwiseAnd(SWT.MODIFIER_MASK) == 0) {
				e.doit = false
				editor.evaluateDocument()
				return
			}
			
			if (e.keyCode == SWT.TAB && e.stateMask.bitwiseAnd(SWT.MODIFIER_MASK) == 0) {
				e.doit = false
				(editor.embeddedEditor.viewer.contentAssistant as ContentAssistant).showPossibleCompletions()
				return
			}
		}
		
	}
	
	private static class GameCommandCommand extends ChangeCommand {
		val SavegameConsoleEditor editor
		val GameCommand command
		
		var String returned

		new(SavegameConsoleEditor editor, GameCommand command) {
			super(editor.editingDomain.resourceSet)
			this.editor = editor
			this.command = command
		}
		
		override protected doExecute() {
			returned = editor.commandEvaluator.evaluate(editor.savegame, command)
		}
		
		def getReturned() {
			returned
		}
	}
}
