package de.tu_bs.isf.mbse.textadventure.player.evaluator

import com.google.inject.ImplementedBy
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.commandDsl.GameCommand
import de.tu_bs.isf.mbse.textadventure.player.commanddsl.validation.CommandDslValidator
import de.tu_bs.isf.mbse.textadventure.savegame.State
import org.eclipse.xtext.validation.IResourceValidator

@ImplementedBy(GameCommandEvaluator)
interface IGameCommandEvaluator {
	/**
	 * Evaluates a given {@link GameCommand} on a {@link State}. The changes described in the command 
	 * are applied directly to the state. Returns a description of the action taken.
	 * 
	 * The command is expected to be valid according to the validation rules defined. Clients must 
	 * take care to validate the command before passing it to the command evaluator, e.g. using the 
	 * {@link CommandDslValidator} or Xtext's {@link IResourceValidator}.
	 */
	def String evaluate(State state, GameCommand command)
}