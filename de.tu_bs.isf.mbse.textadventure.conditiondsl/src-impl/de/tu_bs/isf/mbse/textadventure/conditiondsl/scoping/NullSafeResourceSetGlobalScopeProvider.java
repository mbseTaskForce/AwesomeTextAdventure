package de.tu_bs.isf.mbse.textadventure.conditiondsl.scoping;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.ISelectable;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.GlobalResourceDescriptionProvider;
import org.eclipse.xtext.scoping.impl.ResourceSetGlobalScopeProvider;
import org.eclipse.xtext.scoping.impl.SelectableBasedScope;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;

/**
 * 
 * @see ResourceSetGlobalScopeProvider
 *
 */
public class NullSafeResourceSetGlobalScopeProvider extends ResourceSetGlobalScopeProvider {
	@Inject
	private GlobalResourceDescriptionProvider resourceDecriptionProvider;
	
	protected IScope createScopeWithQualifiedNames(final IScope parent, final Resource resource,
			final Predicate<IEObjectDescription> filter, ResourceSet resourceSet, EClass type, boolean ignoreCase) {
		final Iterable<ISelectable> resourceDescriptionsWithNulls = Iterables.transform(resourceSet.getResources(), new Function<Resource, ISelectable>() {
			@Override
			public ISelectable apply(Resource from) {
				return resourceDecriptionProvider.getResourceDescription(from);
			}
		});
		final Iterable<ISelectable> resourceDescriptions = Iterables.filter(resourceDescriptionsWithNulls, new Predicate<ISelectable>() {
			@Override
			public boolean apply(ISelectable input) {
				return input != null;
			}
		});
		ISelectable compound = new ISelectable() {
			
			@Override
			public boolean isEmpty() {
				for (ISelectable description: resourceDescriptions) {
					if (!description.isEmpty())
						return false;
				}
				return true;
			}
			
			@Override
			public Iterable<IEObjectDescription> getExportedObjectsByType(final EClass type) {
				return Iterables.concat(Iterables.transform(resourceDescriptions, new Function<ISelectable, Iterable<IEObjectDescription>>() {
					@Override
					public Iterable<IEObjectDescription> apply(ISelectable from) {
						return from.getExportedObjectsByType(type);
					}
				}));
			}
			
			@Override
			public Iterable<IEObjectDescription> getExportedObjectsByObject(final EObject object) {
				return Iterables.concat(Iterables.transform(resourceDescriptions, new Function<ISelectable, Iterable<IEObjectDescription>>() {
					@Override
					public Iterable<IEObjectDescription> apply(ISelectable from) {
						return from.getExportedObjectsByObject(object);
					}
				}));
			}
			
			@Override
			public Iterable<IEObjectDescription> getExportedObjects(final EClass type, final QualifiedName name, final boolean ignoreCase) {
				return Iterables.concat(Iterables.transform(resourceDescriptions, new Function<ISelectable, Iterable<IEObjectDescription>>() {
					@Override
					public Iterable<IEObjectDescription> apply(ISelectable from) {
						return from.getExportedObjects(type, name, ignoreCase);
					}
				}));
			}
			
			@Override
			public Iterable<IEObjectDescription> getExportedObjects() {
				return Iterables.concat(Iterables.transform(resourceDescriptions, new Function<ISelectable, Iterable<IEObjectDescription>>() {
					@Override
					public Iterable<IEObjectDescription> apply(ISelectable from) {
						return from.getExportedObjects();
					}
				}));
			}
		};
		return SelectableBasedScope.createScope(parent, compound, filter, type, ignoreCase);
	}
}
