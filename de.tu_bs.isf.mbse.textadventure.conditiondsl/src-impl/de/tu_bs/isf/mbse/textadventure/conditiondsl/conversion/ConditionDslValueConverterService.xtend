package de.tu_bs.isf.mbse.textadventure.conditiondsl.conversion

import com.google.inject.Inject
import org.eclipse.xtext.common.services.DefaultTerminalConverters
import org.eclipse.xtext.conversion.IValueConverter
import org.eclipse.xtext.conversion.ValueConverter
import de.tu_bs.isf.mbse.textadventure.xtext.shared.BareStringValueConverter

class ConditionDslValueConverterService extends DefaultTerminalConverters {
	
	@Inject BareStringValueConverter bareStringValueConverter
	
	@ValueConverter(rule = "BareString")
	def IValueConverter<String> getBareStringValueConverter() {
		bareStringValueConverter
	}
}