package de.tu_bs.isf.mbse.textadventure.adventure.provider

import org.eclipse.emf.common.notify.AdapterFactory
import de.tu_bs.isf.mbse.textadventure.adventure.provider.util.ConditionDslTextHelper

class OrConditionItemProviderMy extends OrConditionItemProvider {
	
	new(AdapterFactory adapterFactory) {
		super(adapterFactory)
	}
	
	override getText(Object obj) {
		ConditionDslTextHelper.instance.getText(obj)
	}
}