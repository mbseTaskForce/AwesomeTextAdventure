package de.tu_bs.isf.mbse.textadventure.adventure.provider

import de.tu_bs.isf.mbse.textadventure.adventure.provider.util.ConditionDslTextHelper
import org.eclipse.emf.common.notify.AdapterFactory

class MessageItemProviderMy extends MessageItemProvider {
	new(AdapterFactory adapterFactory) {
		super(adapterFactory)
	}
	
	override getText(Object obj) {
		ConditionDslTextHelper.instance.getText(obj)
	}
}