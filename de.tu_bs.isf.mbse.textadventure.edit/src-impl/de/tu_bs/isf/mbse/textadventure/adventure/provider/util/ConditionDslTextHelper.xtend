package de.tu_bs.isf.mbse.textadventure.adventure.provider.util

import com.google.inject.Inject
import com.google.inject.Provider
import de.tu_bs.isf.mbse.textadventure.adventure.Condition
import de.tu_bs.isf.mbse.textadventure.adventure.Message
import de.tu_bs.isf.mbse.textadventure.conditiondsl.conditionDsl.ConditionDslFactory
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.internal.ConditionDIFacade
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.ResourceSetReferencingResourceSet
import org.eclipse.xtext.serializer.ISerializer

class ConditionDslTextHelper {
	def static getInstance() {
		instance
	}
	
	val static instance = ConditionDIFacade.defaultInjector.getInstance(ConditionDslTextHelper)
	
	@Inject ISerializer serializer
	@Inject Provider<ResourceSetReferencingResourceSet> resourceSetProvider
	@Inject FileExtensionProvider ext
	
	def getText(Object obj) {
		if (obj instanceof EObject) {
			val copy = EcoreUtil.copy(obj)
			
			val root = obj.createRoot()
			
			switch (copy) {
				Condition: root.condition = copy
				Message: root.messages += copy
			}
			
			try {				
				return serializer.serialize(copy)
			} catch (Exception e) {
				return "[Invalid]"
			}
		}
				
		return ""
	}
	
	def private createRoot(EObject obj) {
		val resSet = resourceSetProvider.get
		resSet.referencedResourceSets.add(obj.eResource.resourceSet)
		val res = resSet.createResource(URI.createURI('''synthetic:/conditon.«ext.getPrimaryFileExtension()»'''))
		val root = ConditionDslFactory.eINSTANCE.createRoot
		res.contents.add(root)
		
		return root
	}
}