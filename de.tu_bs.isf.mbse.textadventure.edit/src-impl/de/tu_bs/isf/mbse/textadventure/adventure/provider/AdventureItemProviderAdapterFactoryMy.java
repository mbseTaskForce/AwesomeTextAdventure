package de.tu_bs.isf.mbse.textadventure.adventure.provider;

import org.eclipse.emf.common.notify.Adapter;

public class AdventureItemProviderAdapterFactoryMy extends AdventureItemProviderAdapterFactory {
	public Adapter createAndConditionAdapter() {
		if (andConditionItemProvider == null) {
			andConditionItemProvider = new AndConditionItemProviderMy(this);
		}

		return andConditionItemProvider;
	}

	public Adapter createOrConditionAdapter() {
		if (orConditionItemProvider == null) {
			orConditionItemProvider = new OrConditionItemProviderMy(this);
		}

		return orConditionItemProvider;
	}

	public Adapter createNotConditionAdapter() {
		if (notConditionItemProvider == null) {
			notConditionItemProvider = new NotConditionItemProviderMy(this);
		}

		return notConditionItemProvider;
	}

	public Adapter createHasItemConditionAdapter() {
		if (hasItemConditionItemProvider == null) {
			hasItemConditionItemProvider = new HasItemConditionItemProviderMy(this);
		}

		return hasItemConditionItemProvider;
	}

	public Adapter createHasExaminedItemConditionAdapter() {
		if (hasExaminedItemConditionItemProvider == null) {
			hasExaminedItemConditionItemProvider = new HasExaminedItemConditionItemProviderMy(this);
		}

		return hasExaminedItemConditionItemProvider;
	}

	public Adapter createHasUsedItemConditionAdapter() {
		if (hasUsedItemConditionItemProvider == null) {
			hasUsedItemConditionItemProvider = new HasUsedItemConditionItemProviderMy(this);
		}

		return hasUsedItemConditionItemProvider;
	}

	public Adapter createIsInRoomConditionAdapter() {
		if (isInRoomConditionItemProvider == null) {
			isInRoomConditionItemProvider = new IsInRoomConditionItemProviderMy(this);
		}

		return isInRoomConditionItemProvider;
	}

	public Adapter createHasVisitedRoomConditionAdapter() {
		if (hasVisitedRoomConditionItemProvider == null) {
			hasVisitedRoomConditionItemProvider = new HasVisitedRoomConditionItemProviderMy(this);
		}

		return hasVisitedRoomConditionItemProvider;
	}
	
	public Adapter createMessageAdapter() {
		if (messageItemProvider == null) {
			messageItemProvider = new MessageItemProviderMy(this);
		}

		return messageItemProvider;
	}

}
