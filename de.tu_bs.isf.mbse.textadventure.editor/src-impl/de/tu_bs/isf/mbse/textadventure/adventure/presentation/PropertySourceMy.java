package de.tu_bs.isf.mbse.textadventure.adventure.presentation;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.PropertySource;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

public class PropertySourceMy extends PropertySource {

	public PropertySourceMy(Object object, IItemPropertySource itemPropertySource) {
		super(object, itemPropertySource);
	}
	
	protected IPropertyDescriptor createPropertyDescriptor(IItemPropertyDescriptor itemPropertyDescriptor) {
		return new PropertyDescriptorMy(object, itemPropertyDescriptor);
	}
}
