package de.tu_bs.isf.mbse.textadventure.adventure.presentation;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.emf.edit.ui.provider.PropertyDescriptor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;

import de.tu_bs.isf.mbse.textadventure.adventure.Action;
import de.tu_bs.isf.mbse.textadventure.adventure.Adventure;
import de.tu_bs.isf.mbse.textadventure.adventure.AdventurePackage;
import de.tu_bs.isf.mbse.textadventure.adventure.Condition;
import de.tu_bs.isf.mbse.textadventure.adventure.IDescribedElement;
import de.tu_bs.isf.mbse.textadventure.adventure.Item;
import de.tu_bs.isf.mbse.textadventure.adventure.Message;

public class PropertyDescriptorMy extends PropertyDescriptor {

	public PropertyDescriptorMy(Object object, IItemPropertyDescriptor itemPropertyDescriptor) {
		super(object, itemPropertyDescriptor);
	}
	
	public CellEditor createPropertyEditor(Composite composite) {
		Object feature = itemPropertyDescriptor.getFeature(object);

		ResourceSet resSet = ((EObject) object).eResource().getResourceSet();
		
		if (feature == AdventurePackage.Literals.ACTION__CONDITON) {
			Condition condition = ((Action) object).getConditon();
			return new ConditionDialogCellEditor(composite, getLabelProvider(), resSet, condition);
		} 
		
		if (feature == AdventurePackage.Literals.ADVENTURE__WIN_CONDITION) {
			Condition condition = ((Adventure) object).getWinCondition();
			return new ConditionDialogCellEditor(composite, getLabelProvider(), resSet, condition);
		}
		
		if (feature == AdventurePackage.Literals.ADVENTURE__FAIL_CONDITION) {
			Condition condition = ((Adventure) object).getFailCondition();
			return new ConditionDialogCellEditor(composite, getLabelProvider(), resSet, condition);
		}
		
		if (feature == AdventurePackage.Literals.ITEM__VISIBILITY_CONDITION) {
			Condition condition = ((Item) object).getVisibilityCondition();
			return new ConditionDialogCellEditor(composite, getLabelProvider(), resSet, condition);
		}
		
		if (feature == AdventurePackage.Literals.MESSAGE__CONDITION) {
			Condition condition = ((Message) object).getCondition();
			return new ConditionDialogCellEditor(composite, getLabelProvider(), resSet, condition);
		}
		
		if (feature == AdventurePackage.Literals.ACTION__ALLOWED_MESSAGES) {
			List<Message> messages = ((Action) object).getAllowedMessages();
			return new MessagesDialogCellEditor(composite, getLabelProvider(), resSet, messages);
		}
		
		if (feature == AdventurePackage.Literals.ACTION__DENIED_MESSAGES) {
			List<Message> messages = ((Action) object).getDeniedMessages();
			return new MessagesDialogCellEditor(composite, getLabelProvider(), resSet, messages);
		}
		
		if (feature == AdventurePackage.Literals.IDESCRIBED_ELEMENT__MESSAGES) {
			List<Message> messages = ((IDescribedElement) object).getMessages();
			return new MessagesDialogCellEditor(composite, getLabelProvider(), resSet, messages);
		}
		
		return super.createPropertyEditor(composite);
	}

	public ILabelProvider getLabelProvider() {
		final IItemLabelProvider itemLabelProvider = itemPropertyDescriptor.getLabelProvider(object);
		return new LabelProvider() {
			@Override
			public String getText(Object object) {
				return itemLabelProvider.getText(object);
			}

			@Override
			public Image getImage(Object object) {
				return ExtendedImageRegistry.getInstance().getImage(itemLabelProvider.getImage(object));
			}
		};
	}
}
