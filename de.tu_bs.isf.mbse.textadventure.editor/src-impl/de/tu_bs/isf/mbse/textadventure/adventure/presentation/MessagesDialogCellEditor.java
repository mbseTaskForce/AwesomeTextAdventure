package de.tu_bs.isf.mbse.textadventure.adventure.presentation;

import java.util.List;

import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import de.tu_bs.isf.mbse.textadventure.adventure.Message;
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded.MessagesDialog;

public class MessagesDialogCellEditor extends ExtendedDialogCellEditor {

	private final List<Message> messages;
	private final ResourceSet resSet;

	public MessagesDialogCellEditor(Composite composite, ILabelProvider labelProvider, ResourceSet resSet, List<Message> messages) {
		super(composite, labelProvider);
		this.resSet = resSet;
		this.messages = messages;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		MessagesDialog dialog = MessagesDialog.createInstance(cellEditorWindow.getShell(), resSet, messages);
		int result = dialog.open();
		if (result == Dialog.OK) {
			return dialog.getMessages();
		}
		return null;
	}

}