package de.tu_bs.isf.mbse.textadventure.adventure.presentation;

import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import de.tu_bs.isf.mbse.textadventure.adventure.Condition;
import de.tu_bs.isf.mbse.textadventure.conditiondsl.ui.editor.embedded.ConditionDialog;

public class ConditionDialogCellEditor extends ExtendedDialogCellEditor {

	private final Condition condition;
	private final ResourceSet resSet;

	public ConditionDialogCellEditor(Composite composite, ILabelProvider labelProvider, ResourceSet resSet, Condition condition) {
		super(composite, labelProvider);
		this.resSet = resSet;
		this.condition = condition;
	}

	@Override
	protected Object openDialogBox(Control cellEditorWindow) {
		ConditionDialog dialog = ConditionDialog.createInstance(cellEditorWindow.getShell(), resSet, condition);
		int result = dialog.open();
		if (result == Dialog.OK) {
			return dialog.getCondition();
		}
		return null;
	}

}
